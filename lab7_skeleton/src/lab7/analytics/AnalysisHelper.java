/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7.analytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import lab7.entities.Comment;
import lab7.entities.Post;
import lab7.entities.User;
import java.util.*;
import java.io.IOException;

/**
 *
 * @author Richa Singh
 */
public class AnalysisHelper {
    
    //Find Average number of likes per comment
    public void getAvgLikesPerComment() {
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        double numberOfLikes = 0;
        int numberOfComments = 0;
        for (Post post : posts.values()) {
            for (Comment c : post.getComments()) {
                numberOfLikes += c.getLikes();
                numberOfComments += 1;
            }
        }
        double avgLikes = numberOfLikes / numberOfComments;
        System.out.println();
        System.out.println("Average number of likes per comment:" + avgLikes);
    }
public void postWithMostLikes() {
        Map<Integer, Integer> postLikecount = new HashMap<Integer, Integer>();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        for (Post post : posts.values()) {
            for (Comment c : post.getComments()) {
                int likes = 0;
                if (postLikecount.containsKey(post.getPostId())) {
                    likes = postLikecount.get(post.getPostId());
                }
                likes += c.getLikes();
                postLikecount.put(post.getPostId(), likes);

            }

        }

        int max = 0;
        int maxId = 0;
        for (int id : postLikecount.keySet()) {
            if (postLikecount.get(id) > max) {
                max = postLikecount.get(id);
                maxId = id;
            }
        }
        System.out.println("The post with maximum liked comments is:" + max + "\n");

    }

    //Find the post with most comments
    public void postWithMostComments() {
        Post post = DataStore.getDataStore().getPosts().values().stream().max(Comparator.comparing(p -> p.getComments().size())).get();
        System.out.println();
        System.out.println("The post with most comments is: " + post.getComments().size());
    }

    public void usersWithLeastPost() {
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        List<Post> postList = new ArrayList<>(posts.values());
        Collections.sort(postList, new Comparator<Post>() {
            @Override
            public int compare(Post o1, Post o2) {
                return o2.getComments().size() - o1.getComments().size();
            }
        });
        System.out.println();
        System.out.println("The 5 most inactive users based on posts: ");
        for (int i = 0; i < postList.size() && i < 5; i++) {
            System.out.println(postList.get(i).getUserId());
        }
    }
    

   //Top 5 inactive users overall (sum of comments, posts and likes)
    public void top5InactiveUsersOverall() {
        Map<Integer, User> userMap = DataStore.getInstance().getUsers();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, Integer> newUserMap = new HashMap<>();
        for (User user : userMap.values()) {
            newUserMap.put(user.getId(), user.getComments().size());
            for (Comment comment : user.getComments()) {
                newUserMap.put(user.getId(), newUserMap.get(user.getId()) + comment.getLikes());
            }
        
        for (Post post : posts.values()) {
            newUserMap.put(post.getUserId(), (post.getUserId()) + 1);
        }
        List<Map.Entry<Integer, Integer>> list = new LinkedList<>(newUserMap.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });
        HashMap<Integer, Integer> temp = new LinkedHashMap<>();
        for (Map.Entry<Integer, Integer> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        System.out.println();
        System.out.println("Top 5 inactive users on the basis of posts, likes and comments:");
        int count = 0;
        for (Map.Entry<Integer, Integer> entry : temp.entrySet()) {
            if (count >= 5) {
                return;
            }
            System.out.println("User Id = " + entry.getKey() + " Sum Of post, likes and comments = " + entry.getValue());
            count++;
        }
    }
    }
    
    public void usersWithLeastComment() {
        Map<Integer, Integer> userCommentcount = new HashMap<Integer, Integer>();
        Map<Integer, User> userMap = DataStore.getInstance().getUsers();

        List<User> userList = new ArrayList(userMap.values());
        Collections.sort(userList, new Comparator<User>(){
            @Override
            public int compare(User u1, User u2) {
                //so as to get descending list
                return u1.getComments().size() - u2.getComments().size();
            }
        });

        System.out.println("The 5 most inactive Users based on comments:");
        for(int i=0; i < userList.size() && i < 5; i++){
        System.out.println(userList.get(i).getId());
    }

    }
    
    //Top 5 proactive users overall (sum of comments, posts and likes)
    public void top5ProactiveUsersOverall() {
        Map<Integer, User> userMap = DataStore.getInstance().getUsers();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, Integer> newUserMap = new HashMap<>();
        for (User user : userMap.values()) {
            newUserMap.put(user.getId(), user.getComments().size());
            for (Comment comment : user.getComments()) {
                newUserMap.put(user.getId(), newUserMap.get(user.getId()) + comment.getLikes());
            }
        }
        for (Post post : posts.values()) {
            newUserMap.put(post.getUserId(), (post.getUserId()) + 1);
        }
        List<Map.Entry<Integer, Integer>> list = new LinkedList<>(newUserMap.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                return o2.getValue() - o1.getValue();
            }
        });
        HashMap<Integer, Integer> temp = new LinkedHashMap<>();
        for (Map.Entry<Integer, Integer> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        System.out.println();
        System.out.println("Top 5 proactive users by posts, comments and likes:");
        int count = 0;
        for (Map.Entry<Integer, Integer> entry : temp.entrySet()) {
            if (count >= 5) {
                return;
            }
            System.out.println("User Id = " + entry.getKey() + " Sum of post, likes and comments = " + entry.getValue());
            count++;
        }
    }
    
    
    
}
    